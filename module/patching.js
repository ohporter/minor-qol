import {Dice5e} from "/systems/dnd5e/module/dice.js"

let myRoll = async ({ event = {}, parts, data, template, title, speaker, flavor, advantage = true, situational = true,
  fastForward = true, critical = 20, fumble = 1, onClose, dialogOptions, }) => {
  // Handle input arguments
  flavor = flavor || title;
  const rollMode = game.settings.get("core", "rollMode");
  let rolled = false;
  
  // Define inner roll function
  const _roll = function (parts, adv, form) {

    // Modify d20 for advantage or disadvantage
    if (adv === 1) {
      parts[0] = ["2d20kh"];
      flavor = `${title} (Advantage)`;
    } else if (adv === -1) {
      parts[0] = ["2d20kl"];
      flavor = `${title} (Disadvantage)`;
    }


    // Optionally include a situational bonus
    data['bonus'] = form ? form.find('[name="bonus"]').val() : 0;
    if (!data.bonus && parts.indexOf("@bonus") !== -1) parts.pop();

    // Optionally include an ability score selection (used for tool checks)
    const ability = form ? form.find('[name="ability"]'): [];
    if (ability.length && ability.val()) {
      data.ability = ability.val();
      const abl = data.abilities[data.ability];
      if (abl) data.mod = abl.mod;
    }

    // Execute the roll and flag critical thresholds on the d20
    let roll = new Roll(parts.join(" + "), data).roll();
    const d20 = roll.parts[0];
    d20.options.critical = critical;
    d20.options.fumble = fumble;

    // Convert the roll to a chat message and return the roll
    roll.toMessage({
      speaker: speaker,
      flavor: flavor,
      rollMode: form ? form.find('[name="rollMode"]').val() : rollMode
    });
    rolled = true;
    return roll;
  };

  // Modify the roll and handle fast-forwarding
  parts = ["1d20"].concat(parts);
  if (event.shiftKey) return _roll(parts, 0);
  else if (event.altKey) return _roll(parts, 1);
  else if (event.ctrlKey || event.metaKey) return _roll(parts, -1);
  else parts = parts.concat(["@bonus"]);

  // Render modal dialog
  template = template || "systems/dnd5e/templates/chat/roll-dialog.html";
  let dialogData = {
    formula: parts.join(" + "),
    data: data,
    rollMode: rollMode,
    rollModes: CONFIG.rollModes,
    config: CONFIG.DND5E
  };
  const html = await renderTemplate(template, dialogData);

  // Create the Dialog window
  let roll;
  return new Promise(resolve => {
    new Dialog({
      title: title,
      content: html,
      buttons: {
        advantage: {
          label: "Advantage",
          callback: html => roll = _roll(parts, 1, html)
        },
        normal: {
          label: "Normal",
          callback: html => roll = _roll(parts, 0, html)
        },
        disadvantage: {
          label: "Disadvantage",
          callback: html => roll = _roll(parts, -1, html)
        }
      },
      default: "normal",
      close: html => {
        if (onClose) onClose(html, parts, data);
        resolve(rolled ? roll : false)
      }
    }, dialogOptions).render(true);
  })
}

Hooks.once("init", () => {
  if (game.system.data.version === 0.82) {
    console.log("Minor-qol | Patching Dice5e.d20Roll")
    let test = new Proxy( Dice5e.d20Roll, {
      apply: (target, thisvalue, args) =>
              myRoll(...args)
    })
    Dice5e.d20Roll = test;
  }
})

function _onDragItemStart(event) {
  event.stopPropagation();
  const li = event.currentTarget;
  const item = this.actor.getOwnedItem(li.dataset.itemId);
  const dragData = {
    type: "Item",
    actorId: this.actor.id,
    data: item.data
  };
  if (this.actor.isToken) dragData.tokenId = this.actor.token.id;
  event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
}

Hooks.once("ready", () => {
  if (game.data.version === "0.5.1") {
    console.log("Minor-qol | Patching ActorSheet._onDragItemStart")
    let onDragItemStartProxy = new Proxy( ActorSheet.prototype._onDragItemStart, {
      apply: (target, thisvalue, args) =>
        _onDragItemStart.bind(thisvalue)(...args)
    })
    ActorSheet.prototype._onDragItemStart = onDragItemStartProxy;
  }
});