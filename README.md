# Minor QOL for Foundry VTT requires 0.5.3 and dnd5e 0.88

### Main Features ###
* Minor QOL has been almost completely rewritten for v0.4.0 to be more flexible so you to decide how much automation you want.
* minor-qol now uses standard chat cards where they exist.  You can choose almost any combination of the following flags to get the behaviour you want.
* Auto Target. When a template is drawn all tokens inside the template are targeted. (mainly useful for area effect spells). Optionally walls between the base/centre of the template block targetting.
* Auto target ranged spells. If the spell specifies a rand and targets of creature/enemy/ally they will be auto targeted if in range of the caster.
* Auto check hits. When an attack roll is made each targeted token is checked to see if it is hit and a chat card displayed.
* Auto check saves. When damage is rolled for anything that requies a save all targeted tokens are checked to see if they save and a chat card displayed.
* Auto roll damage. When an attack roll is made (or a spell that causes damage is cast) roll the damage for the attack/spell. If no tokens are selected damge is rolled anyway. The mdoule will roll critical damage if the roll that triggered it it was a critical. If the roll was a fumble no damage is rolled.
* Auto apply damage. When damage is rolled it is automatically applied to target tokens. However the previous state is remembered. So if auto check hits is enabled only targets that were hit by the attack take damage. If Auto check saves is enabled the saving throw reduces the damage taken. (minor-qol knows most of the spells that cause no damage on save). Optionally take into account damage immunities when applying damage.
* Use damage immunities takes into account each of the damge types in the damage roll, so 2d6 +5 (slashing) + 1d8 (radiant) + 1d4 bless will be treated as (2d6+5+1d4 slashing) + (1d8 radiant) and damage immunities applied per damage component. This happens for both auto damage and chat damage buttons.
* Add chat damage buttons. When enabled each chat damage card has apply damage buttons attached. Damage immunities are applied (if enabled) but saves are not so you need to deal with this as GM. Damage buttons apply to the selected token (not targeted).
* Speed item rolls. When enabled the item icon on the character sheet will skip the standard roll chat card and roll the attack/cast the spell immediately. The rest of the flags determine how much else is rolled. (AutoRollamage is a good choice when this is enabled)

- For spells - it will place the spell template (if there is one), select the targets (if enabled) and then roll the damage. Spells are cast at the default level. If no spell slot is available the cast will fail, unless choose higher slot is enabled in which case a higher level slot will be used if available (good for warlocks) (also see auto check saves).
-  For weapons - The attack roll is made (auto check hits applies) and if auto damage rolls are enabled it auto rolls the damage (auto check damage applies).

## Sample settings.
* Auto roll damage only. When an attack is rolled the damage roll is always made as well. It know about critical hits/fumbles.
* Auto roll damage and auto check hitss. Will roll attacks and damage rolls and tell you if attack hit.
* Semi automated. **My preferred setting** Speed item rolls, Auto target, auto roll damage, auto check saves, auto check hits, add damage buttons. This will place the area effect template (if appropriate), roll the attack, check if the attack hit, roll any saves required and create roll the damage (knowing about criticals/fumbles) and provide damage application buttons.

## Other things
* A confirmation dialog when you delete an item from the character sheet - enable/disable with the "Item Delete Check" flag. 
* You can enable buttons on the inventory sheet. The other buttons just give a short hand for selecting the same functions as from the chat card and support shift/ctl/alt to avoid the dialogs.
* GM sees all whispers. If true (defaults to false) all whispered messages are displayed in the GM's chat log overriding the default behaviour of foundry.
* Colored borders on chat. If enabled draws a border in the users color around each chat message from that user, you can also have the name highlighted in the same color.
* You can choose to display the target token's name or -???- and it's image in the save/hits chat cards. Coupled with CUB this allows anonymous enemies.
* When displaying hit checks or saving throws the GM always sees the token name and clicking on it selects the token - usueful for damage button application.
* When casting a spell the spell is cast a level choice dialog is displayed.  "Pact Magic" and "Always Prepared" speed cast spells will consume slots from the main spell slot list. Innate spells do not use a spell slot. 
* Auto applied damage creates a chat card for the DM which details the damage applied to the target and an "undo" button so that the DM can reverse the damage applied and do the right thing instead.
* The module also supports speed item rolls on the macro bar. Set the flag "Item macros use speed roll" to true and any new macros created by dragging to the macro bar will be speed item rolls. The speed item macros support shift/ctl/alt. Note if you already have a macro of the same name it will not be overwritten with the new one. Either delete the old and recreate for just edit it using the info below.

* You can create speed item macro rolls by hand, create a script macro and enter (the type and versatile fields are optional - type is useful if you have multiple items with the same name, default behaviour is to just pick the first one it finds that matches the name, versatile if true specifies that you want to make a versatile roll with the weapon, defaults to false) .
```MinorQOL.doMacroRoll(event, "Greataxe", {type: "weapon" verstaile: false})```
* ![macro function](macro-function.png)
* The samples below show no automation (just attack and damage rolls - critical damage will be rolled if appropriate), checking if the attack hits and auto applying damage respectively
*![simple roll](simplest.png) ![roll-no-damage](roll-no-apply.png) ![full roll](luthar.png)

* The attack/damage rolls are displayed using the system chat cards.
* ![spell example 1](saella-fireball-names.png) ![spell example 2](saella-fireball-icons.png)


* ![item buttons](item-buttons.png) 

### Installation Instructions

To install a module, follow these instructions:

Paste https://gitlab.com/tposney/minor-qol/raw/master/module.json into the install module prompt inside Foundry or use the foundy module installer.

### Changelog $$$
0.0.82 fix bug with item buttons being added multiple times.  
If auto roll damage is disabled and speed item roll is enabled a roll damage button is added to the attack roll card.  
0.0.83 add support for DNDBeyondSheet5e for speed item rolls.  
0.0.84 reinstate support for roll buttons on betternpcsheet.  
0.0.85 add option to apply damage but don't display undo damage card.  
0.0.86 Fixed spell cast dialog to support pact spells  
Fixed problem with charge consumption for feats.  

### Bugs
Since this is a major rewrite there are sure to be some problems so don't be surprised. It is also a significant change from the previous version so some oddities might exist.

### Feedback
Shoutout to @Red Reign and @Hooking for lots of great code to look at and borrow - you can have it back any time.
Thanks to @errational for being critical of the previous version and encouraging me to rewrite it. I think this is much better.
Special thanks to @BrotherSharp for all his support with translation files.

